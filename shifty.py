#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
path_here = os.path.dirname(os.path.realpath(__file__))
os.chdir(path_here)
# Lets us not care where shifty runs from
import discord, git
from discord.ext import commands
import sys, traceback
import dice, wounds, snack

class myHelp(commands.DefaultHelpCommand):
    def get_ending_note(self):
        return '```Type `%help <command>` for more info on a command.\nYou can also type `%help <category>` for more info on a category.```'

b = commands.Bot(command_prefix=('%'), case_insensitive=True, help_command=myHelp(dm_help=None))

@b.command()
async def hi(ctx, *args):
    '''The hi command. I'll greet the user.
    '''
    await ctx.send('Hi, <@' + str(ctx.author.id) + '>!')

@b.command()
async def updateshifty(ctx, *args):
    '''Updates the bot. Bot owner only.'''
    if 200669454848360448 == ctx.author.id:
        repo = git.Repo('.')
        repo.remotes.origin.fetch()
        repo.git.reset('--hard','origin/master')
        master = repo.head.reference
        message = master.commit.message.split("\n")[0]
        await ctx.send(f'Updated to commit *{message}*. Restarting!')
        await ctx.bot.logout()
        sys.exit(1)

@b.event
async def on_command_error(ctx, error):
    if type(error) == discord.ext.commands.errors.CommandInvokeError:
        log_chan = ctx.channel
        escaped_message = ctx.message.content.replace("`","<backtick>")
        cmd = f'<#{ctx.message.channel.id}> **{ctx.author.display_name}**: `{escaped_message}`'
        tb = ''.join(traceback.format_tb(error.original.__traceback__))
        error = discord.utils.escape_markdown(f'{repr(error.original)}')
        await log_chan.send(f'{error} in command:\n{cmd}\n```{tb}```')

b.add_cog(dice.Rolls())
b.add_cog(wounds.Wounds())
b.add_cog(snack.Snacks())
# Add all the command cogs

with open('secret') as s:
    token = s.read().strip()
# Read the Discord bot token from a secret file

b.run(token)
# Start the bot, finally!
