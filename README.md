# Shifty [<_<]
[<_<] (aka Shifty) is a Weaver Dice bot. It handles dice rolling and wound rolling, and has a few other features. Shifty is a trimmed-down version of Smiley, which is a fully-featured bot with much more functionality that runs on the official Parahumans Discord.

## Inviting Shifty
[Click here](https://discord.com/oauth2/authorize?client_id=885168029560877066&scope=bot&permissions=68608)

## Using Shifty
Type `%help` to see the list of commands. Type `%help <command>` to get more information about a specific command.

For example: `%help roll`

Run a command by typing `%` (the percent sign) and then the name of the command.

For example: `%roll`

Some commands take arguments, which are more information after the name of the command.

For example: `%altwound moderate bash`
